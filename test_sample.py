import tensorflow as tf
import matplotlib.pyplot as plt
import os
import tensorflow_datasets as tfds


MODEL = int(os.environ.get('MODEL', '7'))
TEST_TEXT = os.environ.get(
    "TEST_TEXT",
    """
Director Danny Boyle (Slumdog Millionaire) and screenwriter Richard Curtis (Love Actually) have divergent cinematic sensibilities, but they meld well enough in this bittersweet, slightly wacky musical fantasy rom-com. 
    """
)


def plot_graphs(history, metric):
    plt.plot(history.history[metric])
    plt.plot(history.history['val_' + metric], '')
    plt.xlabel("Epochs")
    plt.ylabel(metric)
    plt.legend([metric, 'val_' + metric])
    plt.show()


dataset, info = tfds.load('imdb_reviews/subwords8k', with_info=True,as_supervised=True)

encoder = info.features['text'].encoder


model = tf.keras.models.load_model(f'models/model-{MODEL}.h5')


def pad_to_size(vec, size):
    zeros = [0] * (size - len(vec))
    vec.extend(zeros)
    return vec


def sample_predict(sample_pred_text, pad):
    encoded_sample_pred_text = encoder.encode(sample_pred_text)

    if pad:
        encoded_sample_pred_text = pad_to_size(encoded_sample_pred_text, 64)
    encoded_sample_pred_text = tf.cast(encoded_sample_pred_text, tf.float32)
    predictions = model.predict(tf.expand_dims(encoded_sample_pred_text, 0))

    return predictions


print("-------------------------------")
print("Sample : ", TEST_TEXT)
print("-------------------------------")
print()

sample_pred_text = TEST_TEXT
predictions = sample_predict(sample_pred_text, True)
print(predictions)
if predictions[0][0] > 0:
    if predictions[0][0] > 5:
        print("Très positif")
    else:
        print("positif")
if predictions[0][0] < 0:
    if predictions[0][0] < -5:
        print("Très négatif")
    else:
        print("négatif")
print("-------------------------------")
