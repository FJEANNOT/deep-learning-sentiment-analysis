import tensorflow as tf
import matplotlib.pyplot as plt
import os
from sklearn.model_selection import ParameterGrid
import tensorflow_datasets as tfds


BUFFER_SIZE = int(os.environ.get("BUFFER_SIZE", '10000'))
BATCH_SIZE = int(os.environ.get("BATCH_SIZE", '64'))
PER_LAYER = int(os.environ.get("PER_LAYER", '64'))
EPOCHS = int(os.environ.get('EPOCHS', '10'))
DROPOUT = int(os.environ.get('DROPOUT', '0.4'))
TEST_TEXT = os.environ.get(
    "TEST_TEXT",
    'The movie was cool. The animation and the graphics '
                    'were out of this world. I would recommend this movie.'
)

checkpoint_path = "models/checkpoints/cp.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)


def plot_graphs(history, metric):
    plt.plot(history.history[metric])
    plt.plot(history.history['val_' + metric], '')
    plt.xlabel("Epochs")
    plt.ylabel(metric)
    plt.legend([metric, 'val_' + metric])
    plt.show()


dataset, info = tfds.load('imdb_reviews/subwords8k', with_info=True,as_supervised=True)
train_examples, test_examples = dataset['train'], dataset['test']

encoder = info.features['text'].encoder

train_dataset = (train_examples
                 .shuffle(BUFFER_SIZE)
                 .padded_batch(BATCH_SIZE, padded_shapes=([None], [])))

test_dataset = (test_examples
                .padded_batch(
                    BATCH_SIZE,
                    padded_shapes=(
                        [None]
                        , []
                    )
                ))


def create_model(optimizer='adam', per_layers=2, dropout=0.3, model_index=0):
    if os.path.isfile(f'models/model-{str(model_index)}.h5'):
        model = tf.keras.models.load_model(f'models/model-{str(model_index)}.h5')
        print(f'Model `models/model-{str(model_index)}.h5` loaded')
    else:
        model = tf.keras.Sequential([
            tf.keras.layers.Embedding(encoder.vocab_size, per_layers),
            tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(per_layers)),
            tf.keras.layers.Dense(per_layers, activation='relu'),
            tf.keras.layers.Dropout(dropout),
            tf.keras.layers.Dense(1)
        ])

        model.compile(
            loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
            optimizer=optimizer,
            metrics=['accuracy']
        )
        print('New model created')
    return model


# model = KerasClassifier(build_fn=create_model)

epochs = [5, 10, 20]
optimizer = ['SGD', 'RMSprop', 'Adagrad', 'Adam', 'Nadam']
per_layer = [2, 4, 8, 16]
dropout = [0.4, 0.8]
param_grid = dict(optimizer=optimizer, per_layer=per_layer, dropout=dropout, epochs=epochs)

params = (list(ParameterGrid(param_grid)))
print(len(params))
# grid = GridSearchCV(estimator=model, param_grid=param_grid, n_jobs=1, cv=3)

# import numpy as np
# ds = np.fromiter(tfds.as_numpy(train_dataset), np.int)
# X = ds[:,0:8]
# y = ds[:,8]
# grid_result = grid.fit(X, y)
#
# print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_), file=open("models/gridsearch-report.txt", "a"))
# means = grid_result.cv_results_['mean_test_score']
# stds = grid_result.cv_results_['std_test_score']
# params = grid_result.cv_results_['params']
# for mean, stdev, param in zip(means, stds, params):
#     print("%f (%f) with: %r" % (mean, stdev, param), file=open("models/gridsearch-report.txt", "a")

cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
                                                 save_weights_only=True,
                                                 verbose=1)

for param in params:
    model = None
    model = create_model(optimizer=param["optimizer"], per_layers=param["per_layer"], dropout=param["dropout"], model_index=params.index(param))
    history = model.fit(
       train_dataset,
       epochs=param["epochs"],
       validation_data=test_dataset,
       validation_steps=5,
       callbacks=[cp_callback]
    )

    test_loss, test_acc = model.evaluate(test_dataset)

    print('PARAMETERS : {}'.format(param), file=open("models/model-report.txt", "a"))
    print('Test Loss: {}'.format(test_loss), file=open("models/model-report.txt", "a"))
    print('Test Accuracy: {}'.format(test_acc), file=open("models/model-report.txt", "a"))
    model.save(f'models/model-{str(params.index(param))}.h5')


def pad_to_size(vec, size):
    zeros = [0] * (size - len(vec))
    vec.extend(zeros)
    return vec


def sample_predict(sample_pred_text, pad):
    encoded_sample_pred_text = encoder.encode(sample_pred_text)

    if pad:
        encoded_sample_pred_text = pad_to_size(encoded_sample_pred_text, 64)
    encoded_sample_pred_text = tf.cast(encoded_sample_pred_text, tf.float32)
    predictions = model.predict(tf.expand_dims(encoded_sample_pred_text, 0))

    return predictions


print("-------------------------------")
print("Sample : ", TEST_TEXT)
print("-------------------------------")
print()

# sample_pred_text = TEST_TEXT
# predictions = sample_predict(sample_pred_text, pad=False)
# print()
# print("-------------------------------")
# print("SANS PAD")
# print(predictions)
# if predictions[0][0] > 0:
#     if predictions[0][0] > 5:
#         print("Très positif")
#     else:
#         print("positif")
# if predictions[0][0] < 0:
#     if predictions[0][0] < -5:
#         print("Très négatif")
#     else:
#         print("négatif")


sample_pred_text = TEST_TEXT
predictions = sample_predict(sample_pred_text, pad=True)
# print("-------------------------------")
# print("AVEC PAD")
print(predictions)
if predictions[0][0] > 0:
    if predictions[0][0] > 5:
        print("Très positif")
    else:
        print("positif")
if predictions[0][0] < 0:
    if predictions[0][0] < -5:
        print("Très négatif")
    else:
        print("négatif")
print("-------------------------------")
