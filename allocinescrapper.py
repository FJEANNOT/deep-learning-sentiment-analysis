import requests
import csv
from bs4 import BeautifulSoup



i = 0
#récupère toutes les informations de toutes les reviews du film
def ScrapeURL(movie_url):
    req = requests.get(movie_url)
    soup = BeautifulSoup(req.text, "lxml")
    global i

    reviews = []
    movie = ' '.join(soup.find("div", {"class": "titlebar-title titlebar-title-lg"}).text.split())
    for review in soup.find_all("div", {"class": "hred review-card cf"}):
        i = i + 1
        output_row = []
        output_row.append(i)
        output_row.append(movie)
        output_row.append(' '.join(review.find("div",{"class": "meta-title"}).text.split()))
        output_row.append(' '.join(review.find("span", {"class": "stareval-note"}).text.split()))
        output_row.append(' '.join(review.find("span", {"class": "review-card-meta-date light"}).text.replace('Publiée le ','').split()))
        output_row.append(' '.join(review.find("div", {"class": "content-txt review-card-content"}).text.split()))
        reviews.append(output_row)

    return reviews

#récupère les ids de tous les films dans les pages entre start_page et end_page
def getMoviesId(start_page, end_page):
    movie_ids = []

    for p in range(start_page, end_page+1):
        req = requests.get('http://www.allocine.fr/films/?page={}'.format(str(p)))
        soup = BeautifulSoup(req.text, 'lxml')

        movies = soup.find_all('h2', 'meta-title')

        for movie in movies:
            movie_ids.append(movie.a['href'].split('=')[1].split('.')[0])

    return movie_ids

#crée le csv a partir des reviews d'allociné
def BuildCsv():
    try:
        open('AllocineReview.csv', 'r').close()
    except FileNotFoundError as e:
        f = open('AllocineReview.csv', 'w')
        f.write(("""id,movie,author,score,date,text\n"""))
        f.close()
    csvfile = open('AllocineReview.csv', 'a', newline='', encoding='utf-8')

    movie_ids = getMoviesId(int(input("page de départ: ")),int(input("page de fin: ")))
    for movie_id in movie_ids:
        reviews = ScrapeURL('http://www.allocine.fr/film/fichefilm-{}/critiques/spectateurs/'.format(str(movie_id)))
        writer = csv.writer(csvfile)
        writer.writerows(reviews)


#lance le script
BuildCsv()