# Tensorflow sentiment analysis

## Preflight
`conda install matplotlib`  
`conda install tensorflow-gpu`  
`conda install tensorflow-datasets`
`conda install pyyaml`
`conda install h5py`

## Run
`python rnn.py`

## Paramètres à tuner

Ligne 5: `BUFFER_SIZE` Taille du buffer (à augmenter si plantage)
Ligne 6: `BATCH_SIZE` Taille des batch (à diminuer si plantange)
Ligne 7: `PER_LAYER` Nombre de neuronnes par couche (à diminuer si plantage)
